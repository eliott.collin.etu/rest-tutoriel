package fr.ulille.iut.tva.ressource;

import fr.ulille.iut.tva.dto.DetailTauxDto;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.ressource.exception.NiveauTvaInexistantException;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
        return TauxTva.NORMAL.taux;
    }

    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) throws NiveauTvaInexistantException {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux;
        } catch (Exception e) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("{niveauTva}")
    public double getMontantTotal(
            @PathParam("niveauTva") String niveau,
            @QueryParam("somme") double somme) {
        try {
            return new CalculTva().calculerMontant(TauxTva.valueOf(niveau.toUpperCase()), somme);
        } catch (Exception e) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("infotaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
        ArrayList<InfoTauxDto> result = new ArrayList<>();
        for (TauxTva t :
                TauxTva.values()) {
            result.add(new InfoTauxDto(t.name(), t.taux));
        }

        return result;
    }

    @GET
    @Path("details/{taux}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public DetailTauxDto getDetail(
            @PathParam("taux") String taux,
            @QueryParam("somme") double somme) {

        return new DetailTauxDto(somme, TauxTva.valueOf(taux.toUpperCase()));
    }

}
