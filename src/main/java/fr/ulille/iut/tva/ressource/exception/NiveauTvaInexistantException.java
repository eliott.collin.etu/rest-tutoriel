package fr.ulille.iut.tva.ressource.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class NiveauTvaInexistantException extends WebApplicationException {
    private static final long serialVersionUID = 1;

    public NiveauTvaInexistantException() {
        super(Response.status(Response.Status.NOT_ACCEPTABLE).entity("Niveau de TVA inexistant").build());
    }
}
