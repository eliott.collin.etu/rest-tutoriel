package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DetailTauxDto {
    private double montantTotal;
    private double montantTva;
    private TauxTva taux;
    private double somme;

    public DetailTauxDto() {}

    public DetailTauxDto(double somme, TauxTva taux) {
        this.montantTotal = new CalculTva().calculerMontant(taux, somme);
        this.montantTva = taux.taux;
        this.somme = somme;
        this.taux = taux;

    }

    public double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public double getMontantTva() {
        return montantTva;
    }

    public void setMontantTva(double montantTva) {
        this.montantTva = montantTva;
    }

    public double getSomme() {
        return somme;
    }

    public void setSomme(double somme) {
        this.somme = somme;
    }

    public TauxTva getTauxLabel() {
        return taux;
    }

    public void setTauxLabel(TauxTva taux) {
        this.taux = taux;
    }

    public double getTauxValue() {
        return taux.taux;
    }

}
